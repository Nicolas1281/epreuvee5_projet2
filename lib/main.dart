import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

// La fonction principale de l'application Flutter
void main() {
  runApp(const MyApp());
}

// Définition de la classe MyApp qui étend StatelessWidget
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // Méthode build pour construire l'interface utilisateur
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Titre de l'application
      title: 'Flutter Demo',

      // Thème de l'application avec une couleur primaire
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),

      // Page d'accueil de l'application
      home: const MyHomePage(),

      // Définition des routes nommées dans l'application
      routes: {
        '/accueil': (context) => const AccueilPage(userID:  ""),
        '/reservation': (context) => const ReservationPage(userID: ""),
        '/dossier_medical': (context) => const FolderMedicalPage(userID: ""),
      },
    );
  }
}

// Définition de la classe MyHomePage qui étend StatefulWidget
class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  // Création de l'état associé à MyHomePage
  _MyHomePageState createState() => _MyHomePageState();
}

// Classe d'état pour MyHomePage
class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Utilisation de SingleChildScrollView pour permettre le défilement
      body: SingleChildScrollView(
        child: Column(
          children: [
            // Conteneur pour l'AppBar personnalisé
            Container(
              margin: const EdgeInsets.only(top: 16),
              child: AppBar(
                title: Row(
                  children: [
                    // Logo de l'application
                    Image.asset(
                      'images/logo.png',
                      height: 100,
                    ),
                    const SizedBox(width: 16),
                    const Expanded(
                      child: Text(
                        '',
                        textAlign: TextAlign.center,
                      ),
                    ),
                    const Spacer(),
                    // Bouton de connexion
                    ElevatedButton(
                      onPressed: () {
                        // Naviguer vers la page de connexion
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const ConnexionPage()),
                        );
                      },
                      child: const Text('Connexion'),
                    ),
                  ],
                ),
              ),
            ),
            // Conteneur pour le Divider
            Container(
              margin: const EdgeInsets.only(top: 8, bottom: 16),
              child: const Divider(
                color: Color(0xFF556B2F),
                thickness: 2,
              ),
            ),
            // Contenu principal centré
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  // Texte de bienvenue
                  const Text(
                    'Bienvenue sur Milmedcare',
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 16),
                  // Texte de description
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 16),
                    child: const Text(
                      'MilMedCare est votre application dédiée à la gestion de vos rendez-vous médicaux et de votre dossier médical. Vous pourrez consulter vos rendez-vous à venir, accéder à vos diagnostics, vos résultats d\'examens et bien plus encore !',
                      textAlign: TextAlign.center,
                    ),
                  ),
                  const SizedBox(height: 16),
                  // Image illustrative
                  Image.asset(
                    'images/image1.jpg',
                    height: 300,
                  ),
                  const SizedBox(height: 16),
                  // Section "Comment ça marche ?"
                  const Text(
                    'Comment ça marche ?',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 8),
                  // Liste à puce avec des instructions
                  Container(
                    margin:
                        const EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    child: const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '\u2022 Connectez-vous avec vos identifiants',
                          style: TextStyle(fontSize: 16),
                        ),
                        Text(
                          '\u2022 Consultez vos prochains rendez-vous',
                          style: TextStyle(fontSize: 16),
                        ),
                        Text(
                          '\u2022 Accédez à votre dossier médical et votre profil',
                          style: TextStyle(fontSize: 16),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 16),
                  // Section "Pourquoi choisir MilMedCare?"
                  const Text(
                    'Pourquoi choisir MilMedCare?',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 8),
                  // Liste à puce avec des avantages
                  Container(
                    margin:
                        const EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    child: const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '\u2022 Gestion simplifiée de vos rendez-vous médicaux',
                          style: TextStyle(fontSize: 16),
                        ),
                        Text(
                          '\u2022 Accès rapide à vos diagnostics et résultats d\'examens',
                          style: TextStyle(fontSize: 16),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 16),
                  // Section "Besoin d'aide ?"
                  const Text(
                    'Besoin d\'aide ?',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 8),
                  // Texte de contact pour assistance
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 16),
                    child: const Text(
                      'Pour toute question ou assistance technique, n\'hésitez pas à nous contacter.',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ConnexionPage extends StatefulWidget {
  const ConnexionPage({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _ConnexionPageState createState() => _ConnexionPageState();
}

class _ConnexionPageState extends State<ConnexionPage> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 90,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.only(top: 16),
              child: Image.asset(
                'images/logo.png',
                height: 100,
              ),
            ),
            const SizedBox(width: 40),
          ],
        ),
        actions: const [
          SizedBox(width: 16),
        ],
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(8),
          child: Container(
            margin: const EdgeInsets.only(top: 8),
            child: const Divider(
              color: Color(0xFF556B2F),
              thickness: 2,
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Column(
                children: [
                  const Text(
                    'Bienvenue sur Milmedcare',
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 16),
                  Image.asset(
                    'images/image2.jpg',
                    height: 200,
                  ),
                  const SizedBox(height: 16),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 16),
                    child: const Text(
                      'Veuillez vous connecter à votre compte pour accéder à votre espace personnel.',
                      textAlign: TextAlign.center,
                    ),
                  ),
                  const SizedBox(height: 16),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 16),
                    padding: const EdgeInsets.all(16),
                    decoration: BoxDecoration(
                      border:
                          Border.all(color: const Color(0xFF556B2F), width: 2),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Form(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          TextFormField(
                            controller: emailController,
                            decoration: const InputDecoration(
                              labelText: 'Adresse e-mail',
                            ),
                          ),
                          const SizedBox(height: 16),
                          TextFormField(
                            controller: passwordController,
                            obscureText: true,
                            decoration: const InputDecoration(
                              labelText: 'Mot de passe',
                            ),
                          ),
                          const SizedBox(height: 24),
                          ElevatedButton(
                            onPressed: () async {
                              String email = emailController.text;
                              String password = passwordController.text;

                              if (email.isEmpty || password.isEmpty) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    content: Text(
                                        'Veuillez remplir tous les champs.'),
                                  ),
                                );
                                return;
                              }

                              var apiUrl = Uri.parse(
                                  'https://milmedcare.nicolaschabaud.com/API/account/accounts.php?action=login');

                              var formData = {
                                'email': email,
                                'password': password,
                              };

                              try {
                                var response =
                                    await http.post(apiUrl, body: formData);

                                if (response.statusCode == 200) {
                                  var data = json.decode(response.body);
                                  if (data["success"]) {
                                    String userID = data["userID"];
                                    // ignore: use_build_context_synchronously
                                    Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            AccueilPage(userID: userID),
                                      ),
                                    );
                                  } else {
                                    // ignore: use_build_context_synchronously
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(data["message"]),
                                      ),
                                    );
                                  }
                                } else {
                                  // ignore: avoid_print
                                  print(
                                      'Échec de la connexion. Code de statut: ${response.statusCode}');
                                }
                              } catch (error) {
                                // ignore: avoid_print
                                print('Erreur lors de la connexion: $error');
                              }
                            },
                            style: ElevatedButton.styleFrom(
                              foregroundColor: Colors.black,
                              backgroundColor: const Color(0xFF8f9779),
                            ),
                            child: const Text('Se connecter'),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AccueilPage extends StatefulWidget {
  final String userID;

  const AccueilPage({Key? key, required this.userID}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _AccueilPageState createState() => _AccueilPageState();
}

class _AccueilPageState extends State<AccueilPage> {
  Map<String, dynamic>? userData;

  @override
  void initState() {
    super.initState();
    fetchUserData();
  }

  Future<void> fetchUserData() async {
    try {
      var apiUrl = Uri.parse(
          'https://milmedcare.nicolaschabaud.com/API/account/read.php?action=read&id=${widget.userID}');
      var response = await http.get(apiUrl);

      if (response.statusCode == 200) {
        // ignore: avoid_print
        print('Data successfully fetched from API.');

        setState(() {
          userData = json.decode(response.body);
        });

        // ignore: avoid_print
        print('User data set: $userData');
      } else {
        // ignore: avoid_print
        print('Failed to fetch data. Status code: ${response.statusCode}');
      }
    } catch (error) {
      // ignore: avoid_print
      print('Error during data fetch: $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 95,
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 16),
                  child: Image.asset(
                    'images/logo.png',
                    height: 100,
                  ),
                ),
                const SizedBox(width: 16),
              ],
            ),
            Row(
              children: [
                IconButton(
                  icon: const Icon(Icons.logout),
                  onPressed: () {
                    Navigator.pushReplacementNamed(context, '/connexion');
                  },
                ),
                const SizedBox(width: 10),
              ],
            ),
          ],
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(8),
          child: Container(
            margin: const EdgeInsets.only(top: 8),
            child: const Divider(
              color: Color(0xFF556B2F),
              thickness: 2,
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                'Mon Profil',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 8),
              Text(
                'Bienvenue ${userData?['userFirstName']}',
                style: const TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 16),
              buildUserInfoCard(),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            label: 'Réservation',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.folder),
            label: 'Dossier Médical',
          ),
        ],
        onTap: (index) {
          // Handle navigation based on the selected item index
          switch (index) {
            case 0:
              // Navigate to the AccueilPage with userID
              // ignore: unnecessary_null_comparison
              if (widget.userID != null) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AccueilPage(userID: widget.userID),
                  ),
                );
              }
              break;
            case 1:
              // Navigate to the ReservationPage with userID if it's not null
              // ignore: unnecessary_null_comparison
              if (widget.userID != null) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        ReservationPage(userID: widget.userID),
                  ),
                );
              }
              break;
            case 2:
              // Navigate to the Dossier Médical page with userID if it's not null
              // ignore: unnecessary_null_comparison
              if (widget.userID != null) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        FolderMedicalPage(userID: widget.userID),
                  ),
                );
              }
              break;
          }
        },
      ),
    );
  }

// Fonction utilitaire pour construire la carte d'informations utilisateur
  Widget buildUserInfoCard() {
    return Card(
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
        side: const BorderSide(
          color: Color(0xFF8F9779),
          width: 2,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (userData != null) ...[
              buildUserInfoRow('Nom', userData!['userFullName']),
              buildUserInfoRow('Prénom', userData!['userFirstName']),
              buildUserInfoRow('Date de naissance', userData!['dateOfBirth']),
              buildUserInfoRow('Sexe', userData!['gender']),
              buildUserInfoRow('Groupe sanguin', userData!['bloodGroup']),
              buildUserInfoRow('Allergies', userData!['allergies']),
              buildUserInfoRow('Adresse', userData!['address']),
              buildUserInfoRow('Code postal', userData!['postalCode']),
              buildUserInfoRow('Lieux Affectation', userData!['city']),
              buildUserInfoRow('Téléphone', userData!['phone']),
              buildUserInfoRow('Email', userData!['email']),
              buildUserInfoRow('Numéro de sécurité sociale',
                  userData!['socialSecurityNumber']),
            ] else
              Container(), // Retourne un conteneur vide
          ],
        ),
      ),
    );
  }

  // Fonction utilitaire pour construire chaque ligne d'information utilisateur
  Widget buildUserInfoRow(String label, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 6.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          // Étiquette
          // ignore: sized_box_for_whitespace
          Container(
            width: 160, // Ajustez la largeur en fonction de vos besoins
            child: Text(
              '$label:',
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Expanded(
            child: Text(
              value,
              style: const TextStyle(
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ReservationPage extends StatefulWidget {
  final String userID;

  const ReservationPage({Key? key, required this.userID}) : super(key: key);

  @override
  _ReservationPageState createState() => _ReservationPageState();
}

class _ReservationPageState extends State<ReservationPage> {
  List<dynamic> reservations = [];

  @override
  void initState() {
    super.initState();
    _fetchReservations();
  }

  Future<void> _fetchReservations() async {
    try {
      final response = await http.get(Uri.parse('https://milmedcare.nicolaschabaud.com/API/reservation/read.php?action=read_reservation&id=${widget.userID}'));

      if (response.statusCode == 200) {
        final List<dynamic> fetchedReservations = json.decode(response.body);
        setState(() {
          reservations = fetchedReservations;
        });
      } else {
        throw Exception('Échec de la requête : ${response.reasonPhrase}');
      }
    } catch (error) {
      print('Erreur lors de la récupération des réservations : $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 95,
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 16),
                  child: Image.asset(
                    'images/logo.png',
                    height: 100,
                  ),
                ),
                const SizedBox(width: 16),
              ],
            ),
            Row(
              children: [
                IconButton(
                  icon: const Icon(Icons.logout),
                  onPressed: () {
                    Navigator.pushReplacementNamed(context, '/connexion');
                  },
                ),
                const SizedBox(width: 10),
              ],
            ),
          ],
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(8),
          child: Container(
            margin: const EdgeInsets.only(top: 8),
            child: const Divider(
              color: Color(0xFF556B2F),
              thickness: 2,
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Text(
                  'Mes Réservations',
                  style: TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 16),
                const Text(
                  'Bienvenue',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 8),
                const Text(
                  'Nous sommes ravis de vous accueillir sur MilMedCare, votre plateforme de gestion de rendez-vous médicaux. Vous trouverez ci-dessous la liste de vos réservations.',
                  style: TextStyle(fontSize: 16),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 16),
                // Liste des réservations
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: reservations.length,
                  itemBuilder: (context, index) {
                    final reservation = reservations[index];
                    return _buildReservationCard(
                      reservation['reservation_id'],
                      reservation['date'],
                      reservation['heure'],
                      reservation['medecin'],
                      reservation['speciality'],
                      reservation['center'],
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            label: 'Réservation',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.folder),
            label: 'Dossier Médical',
          ),
        ],
        onTap: (index) {
          switch (index) {
            case 0:
              if (widget.userID != null) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AccueilPage(userID: widget.userID),
                  ),
                );
              }
              break;
            case 1:
              if (widget.userID != null) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ReservationPage(userID: widget.userID),
                  ),
                );
              }
              break;
            case 2:
              if (widget.userID != null) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FolderMedicalPage(userID: widget.userID),
                  ),
                );
              }
              break;
          }
        },
      ),
    );
  }

  // Fonction pour créer une carte de réservation
  Widget _buildReservationCard(
      String id,
      String date,
      String time,
      String doctorName,
      String specialty,
      String hospital,
      ) {
    return Card(
      child: ListTile(
        title: Text('Rendez-vous n°$id'),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Date: $date'),
            Text('Heure: $time'),
            Text('Médecin: $doctorName'),
            Text('Spécialité: $specialty'),
            Text('Hôpital: $hospital'),
          ],
        ),
      ),
    );
  }
}

class FolderMedicalPage extends StatefulWidget {
  final String userID;

  const FolderMedicalPage({Key? key, required this.userID}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _FolderMedicalPageState createState() => _FolderMedicalPageState();
}

class _FolderMedicalPageState extends State<FolderMedicalPage> {
  Map<String, dynamic>? userData;
  List<Map<String, dynamic>> antecedents = [];
  List<Map<String, dynamic>> diagnostics = [];
  List<Map<String, dynamic>> examens = [];

  @override
  void initState() {
    super.initState();
    fetchAntecedents();
    fetchDiagnostics();
    fetchExamens();
  }

  Future<void> fetchAntecedents() async {
    try {
      var apiUrl = Uri.parse(
          'https://milmedcare.nicolaschabaud.com/API/antecedent/read.php?action=read_antecedent&id=${widget.userID}');
      var response = await http.get(apiUrl);

      if (response.statusCode == 200) {
        // ignore: avoid_print
        print('Antecedents data successfully fetched from API.');
        setState(() {
          antecedents =
              List<Map<String, dynamic>>.from(json.decode(response.body));
        });
        // ignore: avoid_print
        print('Antecedents data set: $antecedents');
      } else {
        // ignore: avoid_print
        print(
            'Failed to fetch antecedents. Status code: ${response.statusCode}');
      }
    } catch (error) {
      // ignore: avoid_print
      print('Error during antecedents fetch: $error');
    }
  }

  Future<void> fetchDiagnostics() async {
    try {
      var apiUrl = Uri.parse(
          'https://milmedcare.nicolaschabaud.com/API/diagnostics/read.php?action=read_diagnostics&id=${widget.userID}');
      var response = await http.get(apiUrl);

      if (response.statusCode == 200) {
        // ignore: avoid_print
        print('Diagnostics data successfully fetched from API.');
        setState(() {
          diagnostics =
              List<Map<String, dynamic>>.from(json.decode(response.body));
        });
        // ignore: avoid_print
        print('Diagnostics data set: $diagnostics');
      } else {
        // ignore: avoid_print
        print(
            'Failed to fetch diagnostics. Status code: ${response.statusCode}');
      }
    } catch (error) {
      // ignore: avoid_print
      print('Error during diagnostics fetch: $error');
    }
  }

  Future<void> fetchExamens() async {
    try {
      var apiUrl = Uri.parse(
          'https://milmedcare.nicolaschabaud.com/API/examens/read.php?action=read_examens&id=${widget.userID}');
      var response = await http.get(apiUrl);

      if (response.statusCode == 200) {
        // ignore: avoid_print
        print('Examens data successfully fetched from API.');
        setState(() {
          examens = List<Map<String, dynamic>>.from(json.decode(response.body));
        });
        // ignore: avoid_print
        print('Examens data set: $examens');
      } else {
        // ignore: avoid_print
        print('Failed to fetch examens. Status code: ${response.statusCode}');
      }
    } catch (error) {
      // ignore: avoid_print
      print('Error during examens fetch: $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 95,
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 16),
                  child: Image.asset(
                    'images/logo.png',
                    height: 100,
                  ),
                ),
                const SizedBox(width: 16),
              ],
            ),
            Row(
              children: [
                IconButton(
                  icon: const Icon(Icons.logout),
                  onPressed: () {
                    Navigator.pushReplacementNamed(context, '/connexion');
                  },
                ),
                const SizedBox(width: 10),
              ],
            ),
          ],
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(8),
          child: Container(
            margin: const EdgeInsets.only(top: 8),
            child: const Divider(
              color: Color(0xFF556B2F),
              thickness: 2,
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Text(
                  'Mon dossier médical',
                  style: TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 16),
                const Text(
                  'Bienvenue',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 8),
                const Text(
                  'Bienvenue sur la page de votre dossier médical personnel. Ici, vous pouvez consulter toutes les informations concernant votre santé, les diagnostics, les résultats d\'examens, les traitements prescrits et vos antécédents médicaux. Votre dossier médical est confidentiel et accessible uniquement par vous et les professionnels de santé autorisés.',
                  style: TextStyle(fontSize: 16),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 24),
                _buildSectionTitle('Antécédents Médicaux'),
                // ignore: sized_box_for_whitespace
                Container(
                  width: double.infinity, // largeur maximale
                  child: buildAntecedentsCard(antecedents),
                ),
                const SizedBox(height: 24),
                _buildSectionTitle('Diagnostics Médicaux'),
                // ignore: sized_box_for_whitespace
                Container(
                  width: double.infinity, // largeur maximale
                  child: buildDiagnosticsCard(diagnostics),
                ),
                const SizedBox(height: 24),
                _buildSectionTitle('Examens Médicaux'),
                // ignore: sized_box_for_whitespace
                Container(
                  width: double.infinity, // largeur maximale
                  child: buildExamensCard(examens),
                ),
                const SizedBox(height: 24),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            label: 'Réservation',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.folder),
            label: 'Dossier Médical',
          ),
        ],
        onTap: (index) {
          // Handle navigation based on the selected item index
          switch (index) {
            case 0:
              // Navigate to the AccueilPage with userID
              // ignore: unnecessary_null_comparison
              if (widget.userID != null) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AccueilPage(userID: widget.userID),
                  ),
                );
              }
              break;
            case 1:
              // Navigate to the ReservationPage with userID if it's not null
              // ignore: unnecessary_null_comparison
              if (widget.userID != null) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        ReservationPage(userID: widget.userID),
                  ),
                );
              }
              break;
            case 2:
              // Navigate to the Dossier Médical page with userID if it's not null
              // ignore: unnecessary_null_comparison
              if (widget.userID != null) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        FolderMedicalPage(userID: widget.userID),
                  ),
                );
              }
              break;
          }
        },
      ),
    );
  }

  Widget _buildSectionTitle(String title) {
    return Text(
      title,
      style: const TextStyle(
        fontSize: 24,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget buildAntecedentsCard(List<Map<String, dynamic>> antecedents) {
    return Card(
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
        side: const BorderSide(
          color: Color(0xFF8F9779),
          width: 2,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            for (var antecedent in antecedents)
              buildAntecedentRow(
                antecedent['type_antecedent'],
                antecedent['description_antecedent'],
              ),
          ],
        ),
      ),
    );
  }

  Widget buildAntecedentRow(String type, String description) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Type d’antécédent: $type',
          style: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 8),
        Text(
          'Description de l\'antécédent: $description',
          style: const TextStyle(fontSize: 14),
        ),
        const SizedBox(height: 16),
      ],
    );
  }

  Widget buildDiagnosticsCard(List<Map<String, dynamic>> diagnostics) {
    return Card(
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
        side: const BorderSide(
          color: Color(0xFF8F9779),
          width: 2,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            for (var diagnostic in diagnostics)
              buildDiagnosticRow(
                diagnostic['Date'],
                diagnostic['Maladie'],
                diagnostic['Description'],
                diagnostic['Traitement'],
              ),
          ],
        ),
      ),
    );
  }

  Widget buildDiagnosticRow(
      String date, String maladie, String description, String traitement) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Date: $date',
          style: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 8),
        Text(
          'Maladie: $maladie',
          style: const TextStyle(
            fontSize: 16,
          ),
        ),
        const SizedBox(height: 8),
        Text(
          'Description: $description',
          style: const TextStyle(fontSize: 14),
        ),
        const SizedBox(height: 8),
        Text(
          'Traitement: $traitement',
          style: const TextStyle(fontSize: 14),
        ),
        const SizedBox(height: 16),
      ],
    );
  }

  Widget buildExamensCard(List<Map<String, dynamic>> examens) {
    return Card(
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
        side: const BorderSide(
          color: Color(0xFF8F9779),
          width: 2,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            for (var examen in examens)
              buildExamenRow(
                examen['Date'],
                examen['Type_examen'],
                examen['Resultats'],
                examen['Remarques'],
              ),
          ],
        ),
      ),
    );
  }

  Widget buildExamenRow(
      String date, String typeExamen, String resultats, String remarques) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Date: $date',
          style: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 8),
        Text(
          'Type d’examen: $typeExamen',
          style: const TextStyle(
            fontSize: 16,
          ),
        ),
        const SizedBox(height: 8),
        Text(
          'Résultats: $resultats',
          style: const TextStyle(fontSize: 14),
        ),
        const SizedBox(height: 8),
        Text(
          'Remarques: $remarques',
          style: const TextStyle(fontSize: 14),
        ),
        const SizedBox(height: 16),
      ],
    );
  }
}
